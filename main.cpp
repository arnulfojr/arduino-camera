#include "opencv2/opencv.hpp"

#include <iostream>
#include <thread>
#include <fcntl.h>
#include <zconf.h>
#include <inttypes.h>

#define FRAME_PAUSE 100

#define SERIAL_PORT "/dev/tty.usbmodem14621"
#define VIDEO_DEVICE 0

using namespace std;

volatile bool RECORD = false;

/**
 * Reads from the SERIAL_PORT
 */
void readFromPort(const string port) {

    unsigned int openMode = O_RDWR | O_NONBLOCK;
    int device = open(port.c_str(), openMode);

    if (device < 0) {
        cout << "Failed to open device" << endl;
        return;
    }

    // we using ASCII chars
    uint8_t buffer[1];

    int bouncer = 0;
    const int OFF = -5;
    const int ON = 5;

    do {
        ssize_t bytesRead = read(device, buffer, sizeof buffer);
        if (bytesRead > 0) {

            string response = string((char*) buffer, 0, (unsigned long) bytesRead);

            // Remove the sensitivity of the ON/OFF by a bouncer
            int signal = atoi((char *) buffer) == 0 ? -1 : 1;
            if (bouncer < ON && bouncer > OFF) {
                bouncer = bouncer + signal;
            } else if (bouncer == ON) {
                RECORD = true;
                bouncer = signal < 0 ? bouncer + signal : bouncer;
            } else if (bouncer == OFF) {
                RECORD = false;
                bouncer = signal > 0 ? bouncer + signal : bouncer;
            }
        }
    } while (buffer[0] != '2');  // 2 is the end of transmission

    close(device);
}

bool openVideo(cv::VideoCapture *capture, int index) {
    if (capture->isOpened()) {
        return true;
    }

    capture->open(index);
    if (!capture->isOpened()) {
        cout << "Error opening video stream or file" << endl;
        return false;
    }

    return true;
}

void releaseVideo(cv::VideoCapture *capture) {
    if (!capture->isOpened()) {
        return;
    }

    // release the camera
    capture->release();
}

void videoFeed(int index) {

    cout << "Will check if video is possible..." << endl;

    // check if video capture is possible
    cv::VideoCapture capture;
    bool isOk = openVideo(&capture, index);

    if (isOk) {
        capture.release();  // we will not stream yet
        cout << "Video is possible..." << endl;
    }

    while (true) {

        if (!RECORD) {
            releaseVideo(&capture);
            sleep(1);
            continue;
        }

        isOk = openVideo(&capture, index);
        if (!isOk) break;

        cv::Mat frame;
        // Capture frame-by-frame
        capture >> frame;

        // If the frame is empty, break immediately
        if (frame.empty()) break;

        // Display the resulting frame
        cv::imshow("Frame", frame);

        int pressedKey = cv::waitKey(FRAME_PAUSE);

        // break if ESC key was pressed
        if (pressedKey == 27) break;
    }

    // in case we ESC
    releaseVideo(&capture);

    // releaseVideo all windows
    cv::destroyAllWindows();
    cout << "Destroyed all windows" << endl;
}


int main() {

    thread serialThread(readFromPort, SERIAL_PORT);

    sleep(1);  // give some time for the serial to stabilize

    /**
     * Video Feed must stay in main thread!
     * Linux: use /dev/video{n} => default is 0 => /dev/video0
     * macOS: uses Qt index
     * Windows: uses some COMPort, unsure...
     * @link https://www.learnopencv.com/read-write-and-display-a-video-using-opencv-cpp-python/
     */
    videoFeed(VIDEO_DEVICE);

    cout << "Will wait for serial to end" << endl;
    serialThread.join();

    return 0;
}