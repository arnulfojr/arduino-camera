/* Constants */
const unsigned int TRIG_PIN = 5;
const unsigned int ECHO_PIN = 6;
const unsigned int LED_PIN = 2;
const unsigned int MAX_DISTANCE = 120;  // centimeters

/**
 * Calculates the distance from the sensor and the closest front object.
 * Returns the distance in centimeters.
 */
int calculateDistance(unsigned int delayDuration) {
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(delayDuration);  // wait a bit
    digitalWrite(TRIG_PIN, LOW);

    long duration = pulseIn(ECHO_PIN, HIGH);
    int distance = (duration / 2) / 29.1;

    return distance;
}

/**
 * Signals the video feed to stream or not.
 * Writes an ASCII value of "1" when stream should happen, else an ASCII "0"
 */
void signalVideoFeed(bool stream) {
    Serial.print(stream, DEC);
    digitalWrite(LED_PIN, stream ? HIGH : LOW);
}

void setup() {
    /* Set Serial */
    Serial.begin(9600);
    Serial.flush();

    /* Set led pin */
    pinMode(LED_PIN, OUTPUT);
    /* Set sensor pins */
    pinMode(TRIG_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);
}

void loop() {

    long objectDistance = calculateDistance(100);  // with delay 100 msec
    delay(100);
    signalVideoFeed(objectDistance <= MAX_DISTANCE);
}